# Query CIM with SPARQL

> SPARQL is an RDF query language, that is, a semantic query language for databases, able to retrieve and manipulate data stored in Resource Description Framework (RDF) format.

This repo contains some examples how SPARQL can be used to query Common Information Models (CIM).
The example queries should work with all SPARQL 1.1 compatible processors.
We recommend to use the Apache Jena ARQ command line tool `sparql`.

## Instructions

1. Download & Install Apache Jena: http://jena.apache.org/download/
  - Details: [jena-sparql-cli-v1.pdf](jena-sparql-cli-v1.pdf)
3. Run examples:
  - `sparql --query queries/transformers.sparql --data cim/MiniGridTestConfiguration_BC_merged_v3.0.0.xml`
  - `sparql --query queries/node7.sparql --data cim/MiniGridTestConfiguration_BC_merged_v3.0.0.xml`

## Examples

### Transformers

Get all transformers:

```
$ sparql --query queries/transformers.sparql --data cim/MiniGridTestConfiguration_BC_merged_v3.0.0.xml
--------------------
| name | desc      |
====================
| "T3" | "Trafo-5" |
| "T5" | "Trafo-1" |
| "T6" | "Trafo-2" |
| "T2" | "Trafo-3" |
| "T1" | "Trafo-4" |
--------------------
```

### Node-7

Get all devices connected to Node-7

```
$ sparql --query queries/node7.sparql --data cim/MiniGridTestConfiguration_BC_merged_v3.0.0.xml
---------------------------------------------
| name | desc      | type                   |
=============================================
| "T5" | "Trafo-1" | cim:PowerTransformer   |
| "L6" | "Line-4"  | cim:ACLineSegment      |
| "G3" |           | cim:SynchronousMachine |
| "T6" | "Trafo-2" | cim:PowerTransformer   |
---------------------------------------------
```

## Credits

- Steffen Vogel <stvogel@eonerc.rwth-aachen.de>
